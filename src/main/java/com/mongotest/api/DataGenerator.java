package com.mongotest.api;

import com.mongotest.model.Ads;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.Random;

public class DataGenerator {

    static final String[] CATEGORIES = {"58a4b39eca0cdc1ba88d9bf5", "58a4b39eca0cdc1ba88d9bf5", "58a4b3a0ca0cdc1ba88d9bf7", "58a4bc132dc4b813650f9561"};
    static final String[] EMAIL = {"vasja@inbox.lv", "sabina25@inbox.lv", "kiska2017@gmail.com", "king666@gmail.com", "konstantin@gmail.com"};
    static final String[] PHONE = {"26055784", "29875674", "29090000", "29838132", "28577465", "25857463"};
    static final String[] DESCRIPTION = {"This is the best description in the world", "Item description. I cell this item because i need money. If you are concerned you can call me. Thank you", "Hello. This is lazy description", "I want ...", "Hello world. This is the test description", "Hello ..."};
    static final BigDecimal[] PRICE = {BigDecimal.valueOf(100d), BigDecimal.valueOf(99.5d), BigDecimal.valueOf(15d), BigDecimal.valueOf(65d), BigDecimal.valueOf(25.50d), BigDecimal.valueOf(5)};

    public static Ads generateData() {
        Random rnd = new Random();

        Ads ads = new Ads();
        ads.setCategoryId(new ObjectId(CATEGORIES[rnd.nextInt(CATEGORIES.length)]));
        ads.setDescription(DESCRIPTION[rnd.nextInt(DESCRIPTION.length)]);
        ads.setEmail(EMAIL[rnd.nextInt(EMAIL.length)]);
        ads.setPhone(PHONE[rnd.nextInt(PHONE.length)]);
        ads.setPrice(PRICE[rnd.nextInt(PRICE.length)]);

        return ads;
    }
}
