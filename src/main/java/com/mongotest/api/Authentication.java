package com.mongotest.api;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/auth")
public class Authentication {

    @RequestMapping(value = "/")
    public String auth(@RequestParam("login") String login, @RequestParam("password") String password) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(login);
            return JWT.create().withIssuer("auth0")
                    .sign(algorithm);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String args[]) throws UnsupportedEncodingException {

    }

}
