package com.mongotest.api;

import com.mongotest.model.Ads;
import com.mongotest.modelto.AdsTO;
import com.mongotest.repository.AdsRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/ads")
public class AdsController {

    @Autowired
    private AdsRepository adsRepository;

    @RequestMapping(value = "/generateAds", method = RequestMethod.GET)
    public void getAdsById() {
        for (int i = 0; i < 30; i++) {
            adsRepository.save(DataGenerator.generateData());
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public AdsTO getAdsById(@PathVariable("id") String id) {
        return convertToTransferObject(adsRepository.findOne(new ObjectId(id)));
    }

    @RequestMapping(value = "/category/{categoryId}", method = RequestMethod.GET)
    public List<AdsTO> getAdsByCategoryId(@PathVariable("categoryId") String categoryId) {
        Stream<Ads> stream = adsRepository.findByCategoryId(new ObjectId(categoryId)).stream();
        return stream.map(this::convertToTransferObject).collect(Collectors.toList());
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public AdsTO create(@RequestBody AdsTO adsTo) {
        Ads convertedAds = convertToEntity(adsTo);
        convertedAds.setReleaseDate(new Date());
        return convertToTransferObject(adsRepository.save(convertedAds));
    }

    private Ads convertToEntity(AdsTO to) {
        Ads ads = new Ads();
        if (to.getId() != null) {
            ads.setId(new ObjectId(to.getId()));
        }
        ads.setCategoryId(new ObjectId(to.getCategoryId()));
        ads.setDescription(to.getDescription());
        ads.setEmail(to.getEmail());
        ads.setPhone(to.getPhone());
        ads.setPrice(to.getPrice());
        ads.setReleaseDate(to.getReleaseDate());
        ads.setDetails(to.getDetails());
        ads.setImages(to.getImages());
        return ads;
    }

    private AdsTO convertToTransferObject(Ads ads) {
        AdsTO adsTO = new AdsTO();
        adsTO.setId(ads.getId().toHexString());
        adsTO.setCategoryId(ads.getCategoryId().toHexString());
        adsTO.setDescription(ads.getDescription());
        adsTO.setEmail(ads.getEmail());
        adsTO.setPhone(ads.getPhone());
        adsTO.setPrice(ads.getPrice());
        adsTO.setReleaseDate(ads.getReleaseDate());
        adsTO.setDetails(ads.getDetails());
        adsTO.setImages(ads.getImages());
        return adsTO;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String uploadFiles(@RequestParam("file") MultipartFile[] files, RedirectAttributes redirectAttributes) {
        for (MultipartFile file : files) {
            Path path = Paths.get("", file.getOriginalFilename());
            try {
                Files.write(path, file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return "Success";
    }
}
