package com.mongotest.api;

import com.mongotest.modelto.CategoryTO;
import com.mongotest.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CategoryTO getCategoryById(@PathVariable("id") String id) {
        return categoryService.getById(id);
    }

    @RequestMapping(value = "/subcategories/{parentId}", method = RequestMethod.GET)
    public List<CategoryTO> getCategoryByParentId(@PathVariable("parentId") String parentId) {
        return categoryService.getSubcategories(parentId);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<CategoryTO> getAll() {
        return categoryService.getAll();
    }

}