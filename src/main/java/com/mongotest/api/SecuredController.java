package com.mongotest.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecuredController {

    @RequestMapping(value = "/secured/hello", method = RequestMethod.GET)
    public String secured() {
        return "Hello";
    }
}
