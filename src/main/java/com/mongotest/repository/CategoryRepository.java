package com.mongotest.repository;

import com.mongotest.model.Category;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CategoryRepository extends MongoRepository<Category, ObjectId> {

    List<Category> findOneByParentId(ObjectId objectId);

}
