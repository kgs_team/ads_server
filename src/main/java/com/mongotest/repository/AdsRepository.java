package com.mongotest.repository;

import com.mongotest.model.Ads;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AdsRepository extends MongoRepository<Ads, ObjectId> {

    List<Ads> findByCategoryId(ObjectId categoryId);

}
