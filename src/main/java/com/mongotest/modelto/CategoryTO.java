package com.mongotest.modelto;

import java.util.List;
import java.util.Map;

public class CategoryTO {

    public String id;

    public Map<String, String> title;

    public Long adsCount;

    public List<CategoryTO> subcategories;

    public CategoryTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, String> getTitle() {
        return title;
    }

    public void setTitle(Map<String, String> title) {
        this.title = title;
    }

    public Long getAdsCount() {
        return adsCount;
    }

    public void setAdsCount(Long adsCount) {
        this.adsCount = adsCount;
    }

    public List<CategoryTO> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<CategoryTO> subcategories) {
        this.subcategories = subcategories;
    }

    @Override
    public String toString() {
        return "CategoryTO{" +
                "id='" + id + '\'' +
                ", title=" + title +
                ", adsCount=" + adsCount +
                ", subcategories=" + subcategories +
                '}';
    }
}
