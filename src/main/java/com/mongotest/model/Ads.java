package com.mongotest.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Document(collection = "ads")
public class Ads {

    @Id
    private ObjectId id;

    @Field(value = "categoryId")
    private ObjectId categoryId;

    @Field(value = "description")
    private String description;

    @Field(value = "phone")
    private String phone;

    @Field(value = "email")
    private String email;

    @Field(value = "price")
    private BigDecimal price;

    @Field(value = "releaseDate")
    private Date releaseDate;

    @Field(value = "details")
    private Map<String, ?> details;

    @Field(value = "images")
    private List<String> images;

    public Ads() {
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(ObjectId categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Map<String, ?> getDetails() {
        return details;
    }

    public void setDetails(Map<String, ?> details) {
        this.details = details;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "Ads{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", description='" + description + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", price=" + price +
                ", releaseDate=" + releaseDate +
                ", details=" + details +
                ", images=" + images +
                '}';
    }
}
