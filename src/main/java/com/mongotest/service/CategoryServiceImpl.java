package com.mongotest.service;

import com.mongotest.model.Category;
import com.mongotest.modelto.CategoryTO;
import com.mongotest.repository.CategoryRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public List<CategoryTO> getSubcategories(String parentId) {
        Stream<Category> stream = categoryRepository.findOneByParentId(new ObjectId(parentId)).stream();
        return stream.map(this::convertToTransferObject).collect(Collectors.toList());
    }

    @Override
    public CategoryTO getById(String id) {
        return convertToTransferObject(categoryRepository.findOne(new ObjectId(id)));
    }

    @Override
    public List<CategoryTO> getAll() {
        AggregationOperation match = Aggregation.match(Criteria.where("parentId").is(null));
        AggregationOperation lookup = Aggregation.lookup("categories", "_id", "parentId", "subcategories");
        Aggregation aggregation = Aggregation.newAggregation(match, lookup);
        AggregationResults<CategoryTO> aggregate = mongoTemplate.aggregate(aggregation, Category.class, CategoryTO.class);

        return aggregate.getMappedResults();
    }

    private CategoryTO convertToTransferObject(Category category) {
        CategoryTO to = new CategoryTO();
        to.setId(category.getId().toHexString());
        to.setTitle(category.getTitle());
        return to;
    }
}
