package com.mongotest.service;

import com.mongotest.modelto.CategoryTO;

import java.util.List;

public interface CategoryService {

    List<CategoryTO> getSubcategories(String parentId);

    CategoryTO getById(String id);

    List<CategoryTO> getAll();
}
